class Generador {

    miNumero;

    constructor() {

    }

    get getValor() {

        this.miNumero=this.generarNumero();
        return this.miNumero;
    }

    generarNumero() {
        let n;
        n = Math.round(Math.random() * 25 + 1);        
        return n;
    }
}